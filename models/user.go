package models

import (
	"errors"
	"time"
)

//User is
type User struct {
	ID        int
	Email     string
	FirstName string
	LastName  string
	DOB       time.Time
	JoinDate  time.Time
	Address   string
	Score     int
	Password  string
}

// UserList is list of all user
var UserList []User

// PrettyDate is
func (u User) PrettyDate() string {
	return u.DOB.String()

}

func init() {
	var user User
	user.FirstName = "init"
	user.LastName = "user"
	user.Email = "init@user.com"
	user.Password = "123456"
	user.DOB = time.Now()
	user.Address = "Dhaka ,Bangladesh"
	user.JoinDate = time.Now()
	UserList = append(UserList, user)

}

//FindUserFromEmail is
func FindUserFromEmail(email string) (User, error) { //db_func
	var user User

	for i := range UserList {
		if UserList[i].Email == email {
			user = UserList[i]
			break
		}
	}
	if user.Email == "" {
		return user, errors.New("user not found")
	}

	return user, nil
}

//FindUserFromEmailAndPassword is
func FindUserFromEmailAndPassword(email string, password string) (User, error) { ////db_func
	var user User

	for i := range UserList {
		if UserList[i].Email == email {
			if UserList[i].Password == password {
				user = UserList[i]
				break

			}

		}
	}
	if user.Email == "" {
		return user, errors.New("Email or Password does not match ")
	}

	return user, nil
}
