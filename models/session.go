package models

import (
	"errors"
)

//Session is
type Session struct {
	ID           int
	CookieString string
	UserEmail    string
}

// SessionList is list of all the Sessions
var SessionList []Session

//FindUserEmailFromSession is
func FindUserEmailFromSession(cookieString string) (string, error) { //db_func
	var session Session

	for i := range UserList {
		if SessionList[i].CookieString == cookieString {
			session = SessionList[i]
			break
		}
	}
	if session.UserEmail == "" {
		return "", errors.New("Session not found")
	}

	return session.UserEmail, nil
}

//RemoveUserFromSession is
func RemoveUserFromSession(cookieString string) { //db_func
	var index *int

	for i := range UserList {
		if SessionList[i].CookieString == cookieString {
			index = &i
			break
		}
	}
	if index != nil {
		///append(slice[:s], slice[s+1:]...)
		SessionList = append(SessionList[:*index], SessionList[(*index+1):]...)
	}
}
