package controllers

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/ahmad4hat/ecommerce-basic-website-with-go-and-oracle/database"
	"bitbucket.org/ahmad4hat/ecommerce-basic-website-with-go-and-oracle/templates"
	
	"github.com/julienschmidt/httprouter"
)

//IndexPage is
type IndexPage struct {
}

//Index is the response of "/" page
func (c IndexPage) Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	content:= database.HelloFromDb()
	err := templates.Tpl.ExecuteTemplate(w, "index.gohtml", struct{DataFields []string }{content})
	//fmt.Fprint(w, "Welcome!\n")
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
func (c IndexPage) DataRT(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	content:= database.HelloFromDb()
	err := templates.Tpl.ExecuteTemplate(w, "rt_data.gohtml", struct{DataFields []string }{content})
	//fmt.Fprint(w, "Welcome!\n")
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}

// Hello is a test for param
func (c IndexPage) Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintf(w, "hello, %s!\n", ps.ByName("name"))
}
