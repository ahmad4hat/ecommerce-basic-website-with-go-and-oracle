package controllers

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/ahmad4hat/ecommerce-basic-website-with-go-and-oracle/database"
	"bitbucket.org/ahmad4hat/ecommerce-basic-website-with-go-and-oracle/models"
	"bitbucket.org/ahmad4hat/ecommerce-basic-website-with-go-and-oracle/templates"
	"github.com/julienschmidt/httprouter"
	uuid "github.com/satori/go.uuid"
)

func printAlluser() {
	if len(models.UserList) == 0 {
		println("no user to print")
		return
	}
	println("printing all user")
	for i, user := range models.UserList {
		println(i)
		println(user.Email)
		println(user.DOB.String())
	}
}

func printAllSession() {
	if len(models.SessionList) == 0 {
		println("no user to print")
		return
	}
	println("printing all Session #21$")
	for i, session := range models.SessionList {
		print("SESSION - ")
		println(i)
		println(session.UserEmail)
		println(session.CookieString)
	}
}

//AuthPage is
type AuthPage struct {
}

//SignInGet is  the responce of sign in  get request pagepage
func (c AuthPage) SignInGet(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	database.HelloFromDb()
	err := templates.Tpl.ExecuteTemplate(w, "signin.gohtml", nil)
	//fmt.Fprint(w, "Welcome!\n")
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}

//SignUpGet is  the responce of signup  get request pagepage
func (c AuthPage) SignUpGet(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	database.HelloFromDb()
	err := templates.Tpl.ExecuteTemplate(w, "signup.gohtml", nil)
	//fmt.Fprint(w, "Welcome!\n")
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}

//SignInPost is
func (c AuthPage) SignInPost(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	fmt.Println("sign in post request")
	email := r.FormValue("email")
	password := r.FormValue("password")

	user, err := models.FindUserFromEmailAndPassword(email, password)
	if err != nil {

		http.Redirect(w, r, "/signin", 302)
		return
	}

	cookieString := uuid.NewV4().String()
	var session models.Session
	session.CookieString = cookieString
	session.UserEmail = user.Email
	cookie := &http.Cookie{
		Name:  "session",
		Value: cookieString,
	}
	models.SessionList = append(models.SessionList, session)
	http.SetCookie(w, cookie)

	printAlluser()
	printAllSession()

	//@setting a cookie

	//@ retriveing a cookie
	// cookie, error := r.Cookie("naox")
	// if error != nil {
	// 	fmt.Println("not something")
	// 	return
	// }
	// fmt.Println(cookie.Value)

	//@deleting a coookie
	// cookie.MaxAge = -1
	// if error != nil {
	// 	http.Error(w, http.StatusText(400), http.StatusBadRequest)
	// }
	// http.SetCookie(w, cookie)
	http.Redirect(w, r, "/session", 302)
}

//SignUpPost is the response to the signup req
func (c AuthPage) SignUpPost(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	fmt.Println("sign in post request")
	email := r.FormValue("email")
	password := r.FormValue("password")
	dateofBirth := r.FormValue("dateOfBirth")
	firstName := r.FormValue("firstName")
	lastName := r.FormValue("lastName")

	var user models.User
	user.FirstName = firstName
	user.LastName = lastName
	user.Password = password
	user.Email = email

	var err error
	user.DOB, err = time.Parse("2006-01-02", dateofBirth)
	if err != nil {
		return
	}

	var session models.Session
	session.CookieString = uuid.NewV4().String()
	session.UserEmail = user.Email
	models.SessionList = append(models.SessionList, session)
	models.UserList = append(models.UserList, user)

	http.SetCookie(w, &http.Cookie{
		Name:  "session",
		Value: session.CookieString,
	})

	printAlluser()
	printAllSession()

	http.Redirect(w, r, "/hello/"+email+password+"[ "+dateofBirth+" ] "+lastName+firstName, 302)
}

// SessionChecker is unique page that list those logged in
func (c AuthPage) SessionChecker(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	println("session tester page")
	cookie, err := r.Cookie("session")
	if err != nil {
		log.Println("something went wrong at find session cookie")
	}
	email, errSe := models.FindUserEmailFromSession(cookie.Value)
	if errSe != nil {
		log.Println("something went wrong at find email from session", errSe)
	}

	errTemplate := templates.Tpl.ExecuteTemplate(w, "session.tester.gohtml", email)

	if errTemplate != nil {
		log.Fatalln("template didn't execute: ", errTemplate)
	}
}

//Logout out is
func (c AuthPage) Logout(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	cookie, err := r.Cookie("session")
	if err != nil {
		println("logout cookie not found")
		http.Redirect(w, r, "/", 302)
	}
	models.RemoveUserFromSession(cookie.Value)

	cookie = &http.Cookie{
		Name:   "session",
		Value:  "",
		MaxAge: -1,
	}
	http.SetCookie(w, cookie)
	http.Redirect(w, r, "/", 302)
	printAllSession()

}
