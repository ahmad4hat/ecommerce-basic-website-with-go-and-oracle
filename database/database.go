package database

import (
	"database/sql" // offical database sql package
	"fmt"

	_ "github.com/godror/godror" // not using goracle.v2 because its deprecated
)

type rowElement struct {
	grade string
	losal string
	hisal string
}

func (r rowElement) printall() {
	fmt.Println(r.grade, r.losal, r.hisal)
}

func (r rowElement) toString() string {
	String := "[ salgrade ---"+ r.grade +", lowsal : "+r.losal + ", hisal : "+r.hisal+ "  ]"
	return String;
}

//HelloFromDb is a test
func HelloFromDb() []string{
	var allSalString []string
	db, err := sql.Open("godror", "scott/tiger@localhost:1521/ORCLCDB.localdomain") // ("gordor "=database driver ,"scott/tiger.........."=conection string )
	if err != nil {
		fmt.Println(err)
		return allSalString
	}
	defer db.Close() // defer delays this line until the end

	rows, err := db.Query("SELECT * FROM SALGRADE ")
	if err != nil {
		fmt.Println("Error running query")
		fmt.Println(err)
		return allSalString
	}

	//var hecx string

	fmt.Println("garde ** losal ** hisal ")
	var allsalarary []rowElement
	

	for rows.Next() {
		var (
			grade string
			losal string
			hisal string
		)
		rows.Scan(&grade, &losal, &hisal)

		a := rowElement{
			grade: grade,
			losal: losal,
			hisal: hisal,
		}
		allSalString = append(allSalString,a.toString())
		allsalarary = append(allsalarary, a)
	}

	for _, salarayRange := range allsalarary {
		salarayRange.printall()
	}
   
	defer rows.Close()
	return allSalString;

}
