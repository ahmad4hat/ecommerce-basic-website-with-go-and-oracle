package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/ahmad4hat/ecommerce-basic-website-with-go-and-oracle/controllers"
	"github.com/julienschmidt/httprouter"
)

func main() {

	var indexpage controllers.IndexPage
	var authPage controllers.AuthPage
	router := httprouter.New()
	router.GET("/", indexpage.Index)
	router.GET("/hello/:name", indexpage.Hello)
	//@@@@@@  session
	// Sign in
	router.GET("/signin", authPage.SignInGet)
	router.POST("/signin", authPage.SignInPost)
	//signup
	router.GET("/signup", authPage.SignUpGet)
	router.POST("/signup", authPage.SignUpPost)
	//logout
	router.GET("/logout", authPage.Logout) // have to change the request to post later

	router.GET("/session", authPage.SessionChecker)
	router.GET("/rt-data", indexpage.DataRT)
	router.ServeFiles("/css/*filepath", http.Dir("css"))
	fmt.Println("The Server is starting ...")
	log.Fatal(http.ListenAndServe(":8000", router)) //gin --appPort 8000 --all -i run server.go
}
